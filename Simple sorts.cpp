#include <chrono>
#include <iostream>
#include <strstream>

using namespace std;
#define time chrono::time_point<chrono::system_clock>

time Now() { return chrono::system_clock::now(); }

void BubbleSort(int* Array, int Length)
{
    for (int i = 1; i < Length - 1; i++)
    {
        for (int j = 0; j < Length - i; j++)
        {
            if (Array[j] > Array[j + 1])
            {
                swap(Array[j], Array[j + 1]);
            }
        }
    }
}

void InsertionSort(int* Array, int Length)
{
    for (int i = 1; i <= Length - 1; i++)
    {
        int j = i - 1;
        while (j >= 0 && Array[j] > Array[j + 1])
        {
            swap(Array[j], Array[j + 1]);
            j--;
        }
    }
}

void ShellSort(int* Array, int Length)
{
    for (int Distance = Length / 2; Distance > 0; Distance /= 2)
    {
        for (int i = 1; i + Distance < Length; i++)
        {
            int j = i + Distance;
            int MiddleValue = Array[j];

            while (j - Distance >= 0 && Array[j - Distance] > MiddleValue)
            {
                Array[j] = Array[j - Distance];
                j -= Distance;
            }
            Array[j] = MiddleValue;
        }
    }
}

void PrintLine()
{
    cout << "______" << endl;
}

void PrintArray(int* Array, int Length)
{
    for (int i = 0; i < Length; i++)
    {
        cout << Array[i] << endl;
    }
    PrintLine();
}

void InitializeRandomArray(int* Array, int Length)
{
    for (int i = 0; i < Length; i++)
    {
        Array[i] = rand();
    }
}

int main(int argc, char* argv[])
{
    chrono::time_point<chrono::system_clock> StartTime;

    const int Length = 10000;
    auto Array = new int[Length];
    InitializeRandomArray(Array, Length);
    // PrintArray(arr, Length);

    StartTime = Now();
    //  BubbleSort(arr, Length);
    // InsertionSort(arr, Length);
    // ShellSort(Array, Length);
    cout << chrono::duration_cast<std::chrono::milliseconds>(Now() - StartTime).count() << " ms" << endl;
    PrintLine();
    // PrintArray(arr, Length);
    return 0;
}
