﻿| Algorithm      | Length = 100 | Length = 1000 | Length = 10000 |
|----------------|--------------|---------------|----------------|
| Bubble Sort    | 0 ms.        | 5 ms.         | 517 ms.        |    
| Insertion Sort | 0 ms.        | 4 ms.         | 464 ms.        | 
|  Shell Sort    | 0 ms.        | 0 ms.         | 1 ms.          | 